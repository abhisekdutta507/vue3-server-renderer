# Vue.js Server Side Rendering Example

Example project that demonstrates the new Vue.js Server Side Rendering APIs and Capabilities. Please follow the instructions to start the setup.

# Getting Started

Test in:

- Repl.it: [https://repl.it/@Modus/vue3-example-ssr](https://repl.it/@Modus/vue3-example-ssr)

### Project setup
```
npm install
```

### Customize configuration
STEP 1: See [Configuration Reference](https://cli.vuejs.org/config/).

```sh
npm i @vue/server-renderer express webpack-manifest-plugin webpack-node-externals
npm i cross-env --save-dev
```

STEP 2: Create `vue.config.js` file in the project root.
```js
const ManifestPlugin = require('webpack-manifest-plugin')
const nodeExternals = require('webpack-node-externals')

exports.chainWebpack = webpackConfig => {
  if (!process.env.SSR) {
    // This is required for repl.it to play nicely with the Dev Server
    webpackConfig.devServer.disableHostCheck(true)
    return
  }

  webpackConfig
    .entry('app')
    .clear()
    .add('./src/main.server.js')

  webpackConfig.target('node')
  webpackConfig.output.libraryTarget('commonjs2')

  webpackConfig
    .plugin('manifest')
    .use(new ManifestPlugin({ fileName: 'ssr-manifest.json' }))

  webpackConfig.externals(nodeExternals({ allowlist: /\.(css|vue)$/ }))

  webpackConfig.optimization.splitChunks(false).minimize(false)

  webpackConfig.plugins.delete('hmr')
  webpackConfig.plugins.delete('preload')
  webpackConfig.plugins.delete('prefetch')
  webpackConfig.plugins.delete('progress')
  webpackConfig.plugins.delete('friendly-errors')

  // console.log(webpackConfig.toConfig())
}
```

STEP 3: Create `src/main.server.js` and add the below codes.

```js
import App from "./App.vue";

export default App;
```

STEP 4: Create `src/server.js` that should contain the below codes.

```js
const path = require('path')
const express = require('express')
const { createSSRApp } = require('vue')
const { renderToString } = require('@vue/server-renderer')
const manifest = require('../dist/ssr-manifest.json')

const server = express()

const appPath = path.join(__dirname, '../dist', manifest['app.js'])
const App = require(appPath).default

server.use('/img', express.static(path.join(__dirname, '../dist', 'img')))
server.use('/js', express.static(path.join(__dirname, '../dist', 'js')))
server.use('/css', express.static(path.join(__dirname, '../dist', 'css')))
server.use(
  '/favicon.ico',
  express.static(path.join(__dirname, '../dist', 'favicon.ico'))
)

server.get('*', async (req, res) => {
  const app = createSSRApp(App)
  const appContent = await renderToString(app)

  const html = `
  <html>
    <head>
      <title>Vue.js SSR</title>
      <link rel="stylesheet" href="${manifest['app.css']}" />
    </head>
    <body>
      ${appContent}
    </body>
  </html>

  `

  res.end(html)
})

console.log(`
  You can navigate to http://localhost:8080
`)

server.listen(8080)
```

STEP 5: Install `@vue/compiler-sfc` as development dependency.
```sh
npm i @vue/compiler-sfc --save-dev
```

STEP 6: Make sure `@vue/server-renderer`, `@vue/compiler-sfc` and `vue` all have similar versions. If required, update `vue` into `"vue": "^3.0.1"`.

### Create and test a server-side-rendered build

```
npm run ssr
```

\*Note: The `pressr` script will create a SSR build before running `src/server.js`.

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```